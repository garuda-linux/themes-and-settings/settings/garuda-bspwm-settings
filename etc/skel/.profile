export EDITOR=/usr/bin/micro
export BROWSER=firedragon
export TERM=alacritty
export MAIL=thunderbird
export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

sh ~/.config/autostart-scripts/tty-color.sh &
